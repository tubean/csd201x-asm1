package com.funix.edu;

import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    static LinkedList<MyFile> myFiles;
    static String rootPath;
    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        Boolean check = true;
        Boolean loaded = false;
        while (check) {
            System.out.println(">>>> Welcome to my app. What can i help you? (Choose a number)");
            System.out.println("1.Load files");
            System.out.println("2.Sort files By Size");
            System.out.println("3.Sort files By Name");
            System.out.println("4.Search");
            System.out.println("5.Read a file");
            System.out.println("0.Exit");
            try {
                int input = scanIn.nextInt();
                switch (input) {
                    case 1:
                        System.out.println("Which folder you want to load?");
                        String folder = scanIn.next();
                        myFiles = MyAppUntils.loadFiles(folder);
                        if (myFiles != null) {
                            MyAppUntils.listFileDetail(myFiles);
                            rootPath = folder;
                            loaded = true;
                        }
                        break;
                    case 2:
                        if (!loaded) {
                            System.out.println("No file is not loaded!! Please choose another option!");
                        } else {
                            System.out.println("Sort the list of files by using");
                            System.out.println("1.Selection sort");
                            System.out.println("2.Insertion sort");
                            System.out.println("3.Quick sort");
                            int type = scanIn.nextInt();
                            switch (type) {
                                case 1: MyAppUntils.sortFiles(SortType.SELECTION, myFiles);
                                    break;
                                case 2: MyAppUntils.sortFiles(SortType.INSERTTIONSORT, myFiles);
                                    break;
                                case 3: MyAppUntils.sortFiles(SortType.QUICKSORT, myFiles);
                                    break;
                                default:
                                    System.out.println("Please try again!");
                                    break;
                            }
                        }
                        break;
                    case 3:
                        if (!loaded) {
                            System.out.println("No file is not loaded!! Please choose another option!");
                        } else {
                            MyAppUntils.sortFilesByName(myFiles);
                        }
                        break;
                    case 4:
                        if (!loaded) {
                            System.out.println("No file is not loaded!! Please choose another option!");
                        } else {
                            System.out.println("Enter any keyword to search: ");
                            String key = scanIn.next();
                            MyAppUntils.searchFileByKeyword(rootPath,key);
                        }
                        break;
                    case 5:
                        if(!loaded) {
                            System.out.println("No file is not loaded!! Please choose another option!");
                        } else {
                            System.out.println("Enter name of file you want to read: ");
                            String fileName = scanIn.next();
                            System.out.println("File : " + fileName);
                            MyAppUntils.readFile(myFiles,fileName);
                        }
                        break;
                    default:
                        check = false;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                check = false;
            }
        }
        System.out.println(">>Good bye<<");
    }

}
