package com.funix.edu;

public class MyFile {
    private String nameFile;
    private double sizeFile;
    private String path;

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public double getSizeFile() {
        return sizeFile;
    }

    public void setSizeFile(double sizeFile) {
        this.sizeFile = sizeFile;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
