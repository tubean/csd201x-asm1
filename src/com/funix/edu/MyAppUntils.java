package com.funix.edu;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

class MyAppUntils {
    //get information of all text files under given folder name 
    static LinkedList<MyFile> loadFiles(String folder) throws IOException{
        if (Files.exists(Paths.get(folder))) {
            System.out.println("Folder: " + folder);
            Path folderRoot = Paths.get(folder);
            LinkedList<MyFile> listFile = new LinkedList<>();
            LinkedList<Path> resultListPath = new LinkedList<>();
            LinkedList<Path> lstPath = fetchChild(folderRoot, resultListPath);
            createListMyFileFromPath(listFile, lstPath);
            return listFile;
        } else {
            System.out.println("The path not found!!");
            return null;
        }
    }

    static void listFileDetail(List<MyFile> myFileList) throws IOException {
        System.out.println(String.format("%-20s%-10s", "Name", "Size(in byte)"));
        if (myFileList.isEmpty()) {
            System.out.println("List of files is empty");
        } else {
            for (MyFile file : myFileList) {
                System.out.println(String.format("%-20s%-10s", file.getNameFile(), file.getSizeFile()));
            }
        }
    }


    //sort the list of files ascending by size (use selection sort, insertion sort or quick sort)
    static void sortFiles(SortType sortType, LinkedList<MyFile> list) {
        try {
            if (sortType == SortType.SELECTION) {
                listFileDetail(selectionSortBySize(list));
            } else if (sortType == SortType.INSERTTIONSORT) {
                listFileDetail(insertionSortBySize(list));
            } else if (sortType == SortType.QUICKSORT) {
                listFileDetail(quickSortBySize(list,0,list.size()-1));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //sort the list of files ascending by name
    static void sortFilesByName(LinkedList<MyFile> list) throws IOException {
        Collections.sort(list, new Comparator<MyFile>() {
            @Override
            public int compare(MyFile o1, MyFile o2) {
                return Collator.getInstance().compare(o1.getNameFile(),o2.getNameFile());
            }
        });
        listFileDetail(list);
    }

    //search file with keyword

    static void searchFileByKeyword(String pathFolderRoot, String keyword) throws IOException {
        Path folderRoot = Paths.get(pathFolderRoot);
        LinkedList<Path> list = new LinkedList<>();
        fetchChildByKeyWord(folderRoot,keyword,list);
        LinkedList<MyFile> resultList = new LinkedList<>();
        createListMyFileFromPath(resultList, list);
        listFileDetail(resultList);
    }

    // read file chosen

    static void readFile(LinkedList<MyFile> listFile, String fileName) {
        LinkedList<MyFile> list = checkFileExist(listFile, fileName);
        if (list.isEmpty()) {
            System.out.println("File not found");
        } else if (list.size() > 1) {
            System.out.println("Multi files found! Choose extinct file, please!");
        } else if (!list.getFirst().getNameFile().endsWith(".txt")) {
            System.out.println("Sorry you must enter text file!");
        } else {
            Path path = Paths.get(list.getFirst().getPath());
            try {
                Files.lines(path).forEach(System.out::println);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static LinkedList<MyFile> checkFileExist(LinkedList<MyFile> listFile, String fileName) {
        LinkedList<MyFile> myFiles = new LinkedList<>();
        for (MyFile file : listFile) {
            if (file.getNameFile().equals(fileName))
                myFiles.add(file);
        }
        return myFiles;
    }

    private static void createListMyFileFromPath(LinkedList<MyFile> listFile, LinkedList<Path> lstPath) throws IOException {
        for (Path path : lstPath) {
            MyFile myFile = new MyFile();
            myFile.setNameFile(path.getFileName().toString());
            myFile.setPath(path.toString());
            myFile.setSizeFile(Files.size(path));
            listFile.add(myFile);
        }
    }

    private static void fetchChildByKeyWord(Path pathFolderRoot,String keyword, LinkedList<Path> resultListPath) {
        if (Files.isDirectory(pathFolderRoot)) {
            try {
                Path[] listPath = Files.list(pathFolderRoot).filter(path -> Files.isRegularFile(path)).filter(path -> path.toString().contains(keyword)).toArray(Path[]::new);
                Collections.addAll(resultListPath, listPath);
                Files.list(pathFolderRoot).filter(path -> Files.isDirectory(path)).forEach(path -> fetchChildByKeyWord(path, keyword, resultListPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Files.isRegularFile(pathFolderRoot) && pathFolderRoot.toString().contains(keyword)) {
            resultListPath.add(pathFolderRoot);
        }
    }

    private static LinkedList<Path> fetchChild(Path pathFolderRoot, LinkedList<Path> resultListPath) {
        if (Files.isDirectory(pathFolderRoot)) {
            try {
                Path[] listPath = Files.list(pathFolderRoot).filter(path -> Files.isRegularFile(path)).toArray(Path[]::new);
                Collections.addAll(resultListPath, listPath);
                Files.list(pathFolderRoot).filter(path -> Files.isDirectory(path)).forEach(path -> fetchChild(path, resultListPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Files.isRegularFile(pathFolderRoot)) {
            resultListPath.add(pathFolderRoot);
        }
        return resultListPath;
    }

    private static LinkedList<MyFile> selectionSortBySize(LinkedList<MyFile> myFileList) {
        if (myFileList.size() <= 1) return myFileList;
        for (int i = 0; i < myFileList.size(); i++) {
            int index = i;
            for (int j = i + 1; j < myFileList.size(); j++) {
                if (myFileList.get(j).getSizeFile() < myFileList.get(index).getSizeFile()) {
                    index = j;
                }
            }
            MyFile myFile = myFileList.get(index);
            myFileList.set(index, myFileList.get(i));
            myFileList.set(i, myFile);
        }
        return myFileList;
    }

    private static LinkedList<MyFile> insertionSortBySize(LinkedList<MyFile> myFileList) {
        if (myFileList.size() <= 1) return myFileList;
        for (int j = 1; j < myFileList.size(); j++) {
            double key = myFileList.get(j).getSizeFile();
            MyFile myFileKey = myFileList.get(j);
            int i = j - 1;
            while ((i > -1) && (myFileList.get(i).getSizeFile() > key)) {
                myFileList.set(i + 1, myFileList.get(i));
                i--;
            }
            myFileList.set(i + 1, myFileKey);
        }
        return myFileList;
    }

    private static LinkedList<MyFile> quickSortBySize(LinkedList<MyFile> myFileList, int lowerIndex, int higherIndex) {
        int size = myFileList.size();
        if(size <= 1) return myFileList;
        int i = lowerIndex;
        int j = higherIndex;
        double pivot = myFileList.get(i+(j-i)/2).getSizeFile();
        while (i <= j) {
            while (myFileList.get(i).getSizeFile() <  pivot) {
                i++;
            }
            while (myFileList.get(j).getSizeFile() > pivot) {
                j--;
            }
            if (i <= j) {
                MyFile temp = myFileList.get(i);
                myFileList.set(i,myFileList.get(j));
                myFileList.set(j,temp);
                i++;
                j--;
            }
        }
        if(lowerIndex < j) quickSortBySize(myFileList,lowerIndex,j);
        if(i < higherIndex) quickSortBySize(myFileList,i,higherIndex);
        return myFileList;
    }
}
